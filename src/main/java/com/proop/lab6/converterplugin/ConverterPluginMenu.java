package com.proop.lab6.converterplugin;

import com.proop.lab4.plugin.ShapeMenu;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import static javax.swing.JFileChooser.APPROVE_OPTION;

public class ConverterPluginMenu extends ShapeMenu {

    private JFrame parentFrame;

    public ConverterPluginMenu() {
        setText("Converter");
        add(createLoadMenuItem());
    }

    private JMenuItem createLoadMenuItem() {
        JMenuItem loadMenuItem = new JMenuItem("Converter");
        loadMenuItem.addActionListener(e -> {
            JPanel converterPluginForm = new ConverterPluginForm();
            parentFrame.getContentPane().removeAll();
            parentFrame.add(converterPluginForm);
            parentFrame.pack();
            parentFrame.revalidate();
        });

        return loadMenuItem;
    }

    @Override
    public void setParentFrame(JFrame parentFrame) {
        this.parentFrame = parentFrame;
    }
}

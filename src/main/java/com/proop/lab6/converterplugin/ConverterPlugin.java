package com.proop.lab6.converterplugin;

import com.proop.lab4.plugin.ShapeButton;
import com.proop.lab4.plugin.ShapeMenu;
import com.proop.lab4.plugin.ShapePlugin;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class ConverterPlugin implements ShapePlugin {

    @Override
    public List<Class<? extends ShapeButton>> getShapeButtons() {
        return emptyList();
    }

    @Override
    public List<Class<? extends ShapeMenu>> getShapeMenus() {
        return asList(ConverterPluginMenu.class);
    }

}

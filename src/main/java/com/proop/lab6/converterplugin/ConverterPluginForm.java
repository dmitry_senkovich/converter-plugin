package com.proop.lab6.converterplugin;

import com.proop.lab4.json.JsonMarshaller;
import com.proop.lab4.json.JsonUnmarshaller;
import com.proop.lab4.shape.Shape;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import by.bsuir.ptoop.controller.util.ArchivingUtility;
import by.bsuir.ptoop.controller.util.DearchivingUtil;

import static javax.swing.JFileChooser.APPROVE_OPTION;

public class ConverterPluginForm extends JPanel {

    private JsonMarshaller jsonMarshaller = new JsonMarshaller();
    private JsonUnmarshaller jsonUnmarshaller = new JsonUnmarshaller();

    List<Shape> shapes;

    public ConverterPluginForm() {
        initComponents();
    }

    private void fromGzip(MouseEvent e) {
        try {
            JFileChooser jFileChooser = new JFileChooser();
            int returnValue = jFileChooser.showOpenDialog(this);
            if (APPROVE_OPTION == returnValue) {
                File file = jFileChooser.getSelectedFile();
                shapes = (List<Shape>) DearchivingUtil.dearchiveObject(file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - 3));
                JOptionPane.showMessageDialog(this, String.format("Loaded %d shapes!", shapes.size()));
            }
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            JOptionPane.showMessageDialog(this, "Failed to load shapes",
                    "Error while loading shapes",  JOptionPane.ERROR_MESSAGE);
        }
    }

    private void toGzip(MouseEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showSaveDialog(this);
        if (APPROVE_OPTION == returnValue) {
            try {
                ArchivingUtility.archiveObject(fileChooser.getSelectedFile().getAbsolutePath(), shapes);
                JOptionPane.showMessageDialog(this, "Shapes are archived!");
            } catch (IOException exception) {
                exception.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        "Failed to archive shapes",
                        "Error while archiving shapes",  JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void fromJson(MouseEvent e) {
        try {
            JFileChooser jFileChooser = new JFileChooser();
            int returnValue = jFileChooser.showOpenDialog(this);
            if (APPROVE_OPTION == returnValue) {
                File file = jFileChooser.getSelectedFile();
                shapes = jsonUnmarshaller.read(file);
                JOptionPane.showMessageDialog(this, String.format("Loaded %d shapes!", shapes.size()));
            }
        } catch (IOException exception) {
            exception.printStackTrace();
            JOptionPane.showMessageDialog(this, "Failed to load shapes",
                    "Error while loading shapes",  JOptionPane.ERROR_MESSAGE);
        }
    }

    private void toJson(MouseEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showSaveDialog(this);
        if (APPROVE_OPTION == returnValue) {
            try {
                jsonMarshaller.save(fileChooser.getSelectedFile(), shapes);
                JOptionPane.showMessageDialog(this, "Shapes are saved!");
            } catch (IOException exception) {
                exception.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        "Failed to save shapes",
                        "Error while saving shapes",  JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void initComponents() {
        setLayout(null);
        setPreferredSize(new Dimension(230, 90));

        fromGzipButton = new JButton();
        fromGzipButton.setText("From Gzip");
        fromGzipButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                fromGzip(e);
            }
        });
        add(fromGzipButton);
        fromGzipButton.setBounds(10, 10, 100, 30);

        toGzipButton = new JButton();
        toGzipButton.setText("To Gzip");
        toGzipButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                toGzip(e);
            }
        });
        add(toGzipButton);
        toGzipButton.setBounds(120, 10, 100, 30);

        fromJsonButton = new JButton();
        fromJsonButton.setText("From Json");
        fromJsonButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                fromJson(e);
            }
        });
        add(fromJsonButton);
        fromJsonButton.setBounds(10, 50, 100, 30);

        toJsonButton = new JButton();
        toJsonButton.setText("To Json");
        toJsonButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                toJson(e);
            }
        });
        add(toJsonButton);
        toJsonButton.setBounds(120, 50, 100, 30);
    }

    private JButton fromGzipButton;
    private JButton toGzipButton;
    private JButton fromJsonButton;
    private JButton toJsonButton;
}
